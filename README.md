# Trip sorter

Sorts a list of random boarding cards in the correct order.

## Setup

Before using it, you have to fetch dependencies by running [Composer](http://getcomposer.org)
in the project's root directory. 

```bash
composer install
```

## Execute

In order to test it, please execute the following command in the project's root directory of the project:

```bash
php index.php
```
