<?php

namespace TripSorter\Contract;

/**
 * Boarding card interface
 */
interface BoardingCard
{
    /**
     * @return string
     */
    public function getInfo();

    /**
     * @return string
     */
    public function getDeparture();

    /**
     * @return string
     */
    public function getArrival();

    /**
     * @return string
     */
    public function getVehicle();
}
