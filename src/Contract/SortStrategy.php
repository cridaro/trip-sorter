<?php

namespace TripSorter\Contract;

/**
 * Sort strategy interface
 */
interface SortStrategy
{
    /**
     * Sort given array
     *
     * @param array $list
     *
     * @return array
     */
    public function sort(array $list);
}
