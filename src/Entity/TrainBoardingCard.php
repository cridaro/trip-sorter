<?php

namespace TripSorter\Entity;

/**
 * Train boarding card class
 */
class TrainBoardingCard extends AbstractBoardingCard
{
    /** @var string */
    protected $vehicle = 'train';

    /** @var string */
    protected $number;

    /** @var string */
    protected $seat;

    /**
     * Get text description
     *
     * @return string
     */
    public function getInfo()
    {
        return sprintf(
            'Take train %s (seat %s) from %s to %s.',
            $this->getNumber(),
            $this->getSeat(),
            $this->getDeparture(),
            $this->getArrival()
        );
    }

    /**
     * Get train number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Get seat assignment
     *
     * @return string
     */
    public function getSeat()
    {
        return $this->seat;
    }
}
