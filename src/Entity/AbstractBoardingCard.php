<?php

namespace TripSorter\Entity;

use stdClass;
use TripSorter\Contract\BoardingCard;

/**
 * Boarding card abstract class
 */
abstract class AbstractBoardingCard implements BoardingCard
{
    /** @var string */
    protected $departure;

    /** @var string */
    protected $arrival;

    /** @var string */
    protected $vehicle;

    /**
     * Get card summary
     *
     * @return string
     */
    abstract public function getInfo();

    /**
     * Constructor
     *
     * @param stdClass|null $data
     */
    public function __construct(stdClass $data = null)
    {
        if (!is_null($data)) {
            $this->populate($data);
        }
    }

    /**
     * Serializer
     *
     * @return string
     */
    public function __toString()
    {
        $vars = get_object_vars($this);
        ksort($vars);

        return json_encode($vars);
    }

    /**
     * Get departure location
     *
     * @return string
     */
    public function getDeparture()
    {
        return $this->departure;
    }

    /**
     * Get arrival location
     *
     * @return string
     */
    public function getArrival()
    {
        return $this->arrival;
    }

    /**
     * Get vehicle type
     *
     * @return string
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Populate object
     *
     * Initialize object properties with given data.
     *
     * @param stdClass $data
     *
     * @return self
     */
    protected function populate(stdClass $data)
    {
        $objectVars = get_object_vars($this);

        if (!is_array($objectVars)) {
            return $this;
        }

        $allowedKeys = array_keys($objectVars);
        $restrictedKeys = ['vehicle'];

        foreach ($data as $key => $value) {
            if (!in_array($key, $allowedKeys) || in_array($key, $restrictedKeys)) {
                continue;
            }

            $setter = 'set' . ucfirst($key);

            if (method_exists($this, $setter)) {
                $this->$setter($value);
            } else {
                $this->{$key} = $value;
            }
        }

        return $this;
    }
}
