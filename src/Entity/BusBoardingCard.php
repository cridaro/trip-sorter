<?php

namespace TripSorter\Entity;

/**
 * Bus boarding card class
 */
class BusBoardingCard extends AbstractBoardingCard
{
    /** @var string */
    protected $vehicle = 'bus';

    /**
     * Get text description
     *
     * @return string
     */
    public function getInfo()
    {
        return sprintf(
            'Take a bus from %s to %s. No seat assignment.',
            $this->getDeparture(),
            $this->getArrival()
        );
    }
}
