<?php

namespace TripSorter\Entity;

/**
 * Plane boarding card class
 */
class PlaneBoardingCard extends AbstractBoardingCard
{
    /** @var string */
    protected $vehicle = 'plane';

    /** @var string */
    protected $counter;

    /** @var string */
    protected $flight;

    /** @var string */
    protected $gate;

    /** @var string */
    protected $seat;

    /**
     * Get text description
     *
     * @return string
     */
    public function getInfo()
    {
        $message = sprintf(
            'From %s, take flight %s to %s (gate %s, seat %s).',
            $this->getDeparture(),
            $this->getFlight(),
            $this->getArrival(),
            $this->getGate(),
            $this->getSeat()
        );

        if (!empty($this->getCounter())) {
            $message .= PHP_EOL . 'Baggage drop at ticket counter ' . $this->getCounter() . '.';
        } else {
            $message .= PHP_EOL . 'Baggage automatically transferred from last leg.';
        }

        return $message;
    }

    /**
     * Get ticket counter
     *
     * @return string
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * Get flight number
     *
     * @return string
     */
    public function getFlight()
    {
        return $this->flight;
    }

    /**
     * Get gate number
     *
     * @return string
     */
    public function getGate()
    {
        return $this->gate;
    }

    /**
     * Get seat assignment
     *
     * @return string
     */
    public function getSeat()
    {
        return $this->seat;
    }
}
