<?php

namespace TripSorter;

use TripSorter\Contract\BoardingCard;
use TripSorter\Contract\SortStrategy;

/**
 * Trip planner class
 */
class TripPlanner
{
    /** @var BoardingCard[] */
    protected $input;

    /** @var BoardingCard[] */
    protected $output;

    /** @var SortStrategy */
    protected $sorter;

    /**
     * Constructor
     *
     * @param BoardingCard[] $list
     * @param SortStrategy   $sorter
     */
    public function __construct(array $list, SortStrategy $sorter)
    {
        $this->input = $list;
        $this->sorter = $sorter;
    }

    /**
     * Sort boarding cards
     *
     * @return self
     */
    public function sort()
    {
        if (count($this->input) > 1) {
            $this->output = $this->sorter->sort($this->input);
        }

        return $this;
    }

    /**
     * Get trip information
     */
    public function display()
    {
        $count = 1;
        $text = 'TRIP PLAN:' . PHP_EOL;

        foreach ($this->output as $card) {
            $text .= $count . '. ' . $card->getInfo() . PHP_EOL . PHP_EOL;
            $count++;
        }

        $text .= $count . '. You have arrived at your destination.' . PHP_EOL;

        return $text;
    }
}
