<?php

namespace TripSorter\Service;

use Exception;

/**
 * Boarding cards parser class
 */
class BoardingCardsParser
{
    const JSON_EXAMPLE = 'Please provide a valid JSON. Example:
    [
        {
            "departure": "Some place",
            "arrival": "Other place",
            "vehicle": "train",
            "number": "F35",
            "seat": "12C"
        }
    ]';

    /**
     * Parse given file
     *
     * @param string $filename
     *
     * @return mixed
     * @throws Exception
     */
    public function parseFile($filename)
    {
        if (!file_exists($filename)) {
            throw new Exception("File '$filename' could not be found.");
        }

        // read input file
        $input = file_get_contents($filename);

        if (!$input) {
            throw new Exception("File '$filename' could not be read.");
        }

        return $this->parseString($input);
    }

    /**
     * Parse given string
     *
     * @param string $input
     *
     * @return array
     * @throws Exception
     */
    public function parseString($input)
    {
        // validate input
        $this->validate($input);

        // parse input string
        $result = json_decode($input);

        if (json_last_error() > 0) {
            throw new Exception('Invalid input string. ' . self::JSON_EXAMPLE);
        }

        // always return array
        if (!is_array($result)) {
            $result = [$result];
        }

        return $result;
    }

    /**
     * Validate given input
     *
     * @param mixed $input
     *
     * @throws Exception
     */
    protected function validate($input)
    {
        if (!is_string($input)) {
            $type = gettype($input);
            throw new Exception("Invalid input type: string expected, $type given.");
        }

        if (empty($input)) {
            throw new Exception('Empty input string. ' . self::JSON_EXAMPLE);
        }
    }
}
