<?php

namespace TripSorter\Service;

use stdClass;
use Exception;
use TripSorter\Contract\BoardingCard;
use TripSorter\Entity\BusBoardingCard;
use TripSorter\Entity\TrainBoardingCard;
use TripSorter\Entity\PlaneBoardingCard;

/**
 * Boarding card factory class
 */
class BoardingCardFactory
{
    const VEHICLE_TYPE_BUS   = 'bus';
    const VEHICLE_TYPE_TRAIN = 'train';
    const VEHICLE_TYPE_PLANE = 'plane';

    /** @var BoardingCardValidator */
    protected $validator;

    /**
     * Constructor
     *
     * @param BoardingCardValidator $validator
     */
    public function __construct(BoardingCardValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Create boarding card
     *
     * @param stdClass $data
     *
     * @return BoardingCard
     * @throws Exception
     */
    public function create(stdClass $data)
    {
        $this->validator->validateInput($data);
        $vehicle = strtolower($data->vehicle);

        switch ($vehicle) {
            case self::VEHICLE_TYPE_BUS:
                $card = new BusBoardingCard($data);
                break;
            case self::VEHICLE_TYPE_TRAIN:
                $card = new TrainBoardingCard($data);
                break;
            case self::VEHICLE_TYPE_PLANE:
                $card = new PlaneBoardingCard($data);
                break;
            default:
                throw new Exception("Unknown vehicle type: $vehicle.");
        }

        $this->validator->validateEntity($card);

        return $card;
    }
}
