<?php

namespace TripSorter\Service;

use TripSorter\Contract\BoardingCard;
use TripSorter\Contract\SortStrategy;

/**
 * Chain sort strategy class
 *
 * @package TripSorter
 */
class ChainSorter implements SortStrategy
{
    /**
     * Sort boarding cards array
     *
     * @param BoardingCard[] $list
     *
     * @return BoardingCard[]
     */
    public function sort(array $list)
    {
        // init sorted cards list
        $sorted = [];

        // add card to sorted list
        $sorted[] = array_pop($list);

        while (count($list) > 0) {
            foreach ($list as $index => $current) {
                /**
                 * @var BoardingCard $current
                 * @var BoardingCard $firstSorted
                 * @var BoardingCard $lastSorted
                 */
                $firstSorted = reset($sorted);
                $lastSorted = end($sorted);

                if ($current->getDeparture() == $lastSorted->getArrival()) {
                    array_push($sorted, $current);
                    unset($list[$index]);
                } elseif ($current->getArrival() == $firstSorted->getDeparture()) {
                    array_unshift($sorted, $current);
                    unset($list[$index]);
                } else {
                    continue;
                }
            }
        }

        return $sorted;
    }
}
