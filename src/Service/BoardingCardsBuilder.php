<?php

namespace TripSorter\Service;

use stdClass;
use Exception;

/**
 * Boarding cards builder class
 *
 * Builds a list of boarding cards using a JSON string as input.
 */
class BoardingCardsBuilder
{
    const JSON_EXAMPLE = 'Example:
    [
        {
            "departure": "Some place",
            "arrival": "Other place",
            "vehicle": "train",
            "number": "F35",
            "seat": "12C"
        }
    ]';

    /** @var BoardingCardFactory */
    protected $factory;

    /** @var array */
    protected $input;

    /**
     * Constructor
     *
     * @param stdClass[]          $input
     * @param BoardingCardFactory $factory
     */
    public function __construct(array $input, BoardingCardFactory $factory)
    {
        $this->input = $input;
        $this->factory = $factory;
    }

    /**
     * Build boarding cards list
     *
     * @return array
     * @throws Exception
     */
    public function build()
    {
        $result = [];

        foreach ($this->input as $item) {
            $card = $this->factory->create($item);
            $result[] = $card;
        }

        return $result;
    }
}
