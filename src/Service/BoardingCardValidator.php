<?php

namespace TripSorter\Service;

use stdClass;
use Exception;
use TripSorter\Contract\BoardingCard;

/**
 * Boarding card validator class
 */
class BoardingCardValidator
{
    protected static $required = [
        'departure',
        'arrival',
        'vehicle',
    ];

    /**
     * Validate input data
     *
     * @param stdClass $data
     *
     * @return boolean
     * @throws Exception
     */
    public function validateInput(stdClass $data)
    {
        $missing = [];

        if (!is_object($data)) {
            $type = gettype($data);
            throw new Exception("Invalid input data: object expected, $type given.");
        }

        foreach (self::$required as $key) {
            if (!isset($data->{$key}) || empty($data->{$key})) {
                $missing[] = $key;
            }
        }

        if (count($missing)) {
            $fields = implode(', ', $missing);
            throw new Exception("Required fields missing: $fields.");
        }

        return true;
    }

    /**
     * Validate entity data
     *
     * @param BoardingCard $entity
     * @return boolean
     * @throws Exception
     */
    public function validateEntity(BoardingCard $entity)
    {
        $missing = [];

        foreach (self::$required as $key) {
            $getter = 'get' . ucfirst($key);
            $value = null;

            if (method_exists($entity, $getter)) {
                $value = $entity->$getter();
            }

            if (empty($value)) {
                $missing[] = $key;
            }
        }

        if (count($missing)) {
            $fields = implode(', ', $missing);
            throw new Exception("Required properties missing or empty: $fields.");
        }

        return true;
    }
}
