<?php

// define constants
define('APP_DIR', __DIR__);

// setup autoloader
require_once __DIR__ . '/vendor/autoload.php';
