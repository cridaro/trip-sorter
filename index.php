<?php

// setup autoloader
require_once __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

use TripSorter\TripPlanner;
use TripSorter\Service\BoardingCardsBuilder;
use TripSorter\Service\BoardingCardFactory;
use TripSorter\Service\BoardingCardsParser;
use TripSorter\Service\BoardingCardValidator;
use TripSorter\Service\ChainSorter;

try {
    // read input file
    $parser = new BoardingCardsParser();
    $input = $parser->parseFile(__DIR__ . DIRECTORY_SEPARATOR . 'input.json');

    // build cards list
    $builder = new BoardingCardsBuilder($input, new BoardingCardFactory(new BoardingCardValidator()));
    $cards = $builder->build();

    // sort cards list
    $trip = new TripPlanner($cards, new ChainSorter());
    $trip->sort();

    // display trip information
    echo $trip->display();
} catch (Exception $exception) {
    echo $exception->getMessage();
}
