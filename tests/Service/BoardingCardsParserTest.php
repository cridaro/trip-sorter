<?php

namespace TripSorter\Service;

/**
 * Boarding cards parser test
 */
class BoardingCardsParserTest extends \PHPUnit_Framework_TestCase
{
    public function testParseFileSuccess()
    {
        try {
            $parser = new BoardingCardsParser();
            $data = $parser->parseFile(APP_DIR . DIRECTORY_SEPARATOR . 'input.json');
        } catch (\Exception $exception) {
            $data = null;
        }

        self::assertInternalType('array', $data);
        self::assertInternalType('object', $data[0]);
        self::assertCount(4, $data);
    }

    public function testParseStringSuccess()
    {
        try {
            $parser = new BoardingCardsParser();
            $input = '[{"departure": "Rome", "arrival": "Venice", "vehicle": "train"}]';
            $data = $parser->parseString($input);
        } catch (\Exception $exception) {
            $data = null;
        }

        self::assertInternalType('array', $data);
        self::assertInternalType('object', $data[0]);
        self::assertCount(1, $data);
    }

    /**
     * @expectedException \Exception
     */
    public function testParseFileWithBadFilename()
    {
        $parser = new BoardingCardsParser();
        $parser->parseFile('filename.json');
    }

    /**
     * @expectedException \Exception
     */
    public function testParseFileWithBadFile()
    {
        $parser = new BoardingCardsParser();
        $parser->parseFile('/etc/passwd');
    }

    /**
     * @expectedException \Exception
     */
    public function testParseStringWithBadType()
    {
        $parser = new BoardingCardsParser();
        $parser->parseString(123);
    }

    /**
     * @expectedException \Exception
     */
    public function testParseStringWithEmptyString()
    {
        $parser = new BoardingCardsParser();
        $parser->parseString('');
    }

    /**
     * @expectedException \Exception
     */
    public function testParseStringWithInvalidString()
    {
        $parser = new BoardingCardsParser();
        $parser->parseString('invalid string');
    }
}
