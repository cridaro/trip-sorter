<?php

namespace TripSorter\Service;

/**
 * Boarding card factory test
 */
class BoardingCardFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructSuccess()
    {
        $factory = $this->getFactoryObject();

        self::assertInstanceOf('TripSorter\Service\BoardingCardFactory', $factory);
    }

    public function testCreateSuccess()
    {
        $data = new \stdClass();
        $data->departure = 'Paris';
        $data->arrival = 'Lyon';
        $data->vehicle = 'bus';

        try {
            $object = $this->getFactoryObject()->create($data);
        } catch (\Exception $exception) {
            $object = null;
        }

        self::assertInstanceOf('TripSorter\Contract\BoardingCard', $object);
        self::assertInstanceOf('TripSorter\Entity\BusBoardingCard', $object);
    }

    /**
     * @return BoardingCardFactory
     */
    protected function getFactoryObject()
    {
        return new BoardingCardFactory($this->getValidatorMock());
    }

    /**
     * @return BoardingCardValidator
     */
    protected function getValidatorMock()
    {
        /** @var BoardingCardValidator $validator */
        $validator = $this->getMockBuilder('TripSorter\Service\BoardingCardValidator')->getMock();

        return $validator;
    }
}
