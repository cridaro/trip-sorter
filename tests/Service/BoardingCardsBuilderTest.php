<?php

namespace TripSorter\Service;

/**
 * Boarding cards builder test
 */
class BoardingCardsBuilderTest extends \PHPUnit_Framework_TestCase
{
    public function testBuildSuccess()
    {
        $items = json_decode('[{"departure": "New York", "arrival": "London", "vehicle": "plane"}]');

        try {
            $builder = $this->getBuilderObject($items);
            $result = $builder->build();
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            $result = null;
        }

        self::assertInternalType('array', $result);
        self::assertCount(1, $result);
    }

    public function testConstructSuccess()
    {
        $builder = $this->getBuilderObject();

        self::assertInstanceOf('TripSorter\Service\BoardingCardsBuilder', $builder);
    }

    /**
     * @param array $items
     *
     * @return BoardingCardsBuilder
     */
    protected function getBuilderObject($items = [])
    {
        $entity = $this->getMockBuilder('TripSorter\Entity\PlaneBoardingCard')
            ->disableOriginalConstructor()
            ->getMock();
        $factory = $this->getMockBuilder('TripSorter\Service\BoardingCardFactory')
            ->disableOriginalConstructor()
            ->getMock();
        $factory->method('create')->willReturn($entity);

        return new BoardingCardsBuilder($items, $factory);
    }
}
