<?php

namespace TripSorter\Service;

use TripSorter\Entity\BusBoardingCard;
use TripSorter\Entity\PlaneBoardingCard;
use TripSorter\Entity\TrainBoardingCard;

/**
 * Boarding card validator test
 */
class BoardingCardValidatorTest extends \PHPUnit_Framework_TestCase
{
    public function testValidateEntitySuccess()
    {
        try {
            $input = new \stdClass();
            $input->departure = 'Milan';
            $input->arrival = 'Venice';
            $input->vehicle = 'bus';
            $entity = new BusBoardingCard($input);
            $validator = new BoardingCardValidator();
            $result = $validator->validateEntity($entity);
        } catch (\Exception $exception) {
            $result = null;
        }

        self::assertInternalType('boolean', $result);
        self::assertEquals(true, $result);
    }

    public function testValidateInputSuccess()
    {
        try {
            $input = new \stdClass();
            $input->departure = 'Rome';
            $input->arrival = 'Milan';
            $input->vehicle = 'train';
            $validator = new BoardingCardValidator();
            $result = $validator->validateInput($input);
        } catch (\Exception $exception) {
            $result = null;
        }

        self::assertInternalType('boolean', $result);
        self::assertEquals(true, $result);
    }

    /**
     * @expectedException \Exception
     */
    public function testValidateEntityWithEmptyData()
    {
        $entity = new PlaneBoardingCard();
        $validator = new BoardingCardValidator();
        $validator->validateEntity($entity);
    }

    /**
     * @expectedException \Exception
     */
    public function testValidateEntityWithIncompleteData()
    {
        $input = new \stdClass();
        $input->departure = 'Paris';
        $entity = new TrainBoardingCard($input);
        $validator = new BoardingCardValidator();
        $validator->validateEntity($entity);
    }

    /**
     * @expectedException \Exception
     */
    public function testValidateInputWithEmptyData()
    {
        $input = new \stdClass();
        $validator = new BoardingCardValidator();
        $validator->validateInput($input);
    }

    /**
     * @expectedException \Exception
     */
    public function testValidateInputWithIncompleteData()
    {
        $input = new \stdClass();
        $input->arrival = 'Bordeaux';
        $validator = new BoardingCardValidator();
        $validator->validateInput($input);
    }
}
