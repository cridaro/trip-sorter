<?php

namespace TripSorter\Service;

/**
 * Chain sorter test
 */
class ChainSorterTest extends \PHPUnit_Framework_TestCase
{
    /** @var string */
    protected static $input = '[{""}]';

    /** @var string */
    protected static $output;

    public function testSort()
    {
        $inputList = $this->prepareList('input');
        $outputList = $this->prepareList('output');
        $outputJson = array_map('strval', $outputList);

        $sorter = new ChainSorter();
        $sortedList = $sorter->sort($inputList);
        $sortedJson = array_map('strval', $sortedList);

        self::assertInternalType('array', $sortedList);
        self::assertCount(4, $sortedList);
        self::assertEquals($outputJson, $sortedJson);
    }

    /**
     * @param string $file
     * @param string $type
     *
     * @return array
     */
    protected function prepareList($file)
    {
        try {
            $parser = new BoardingCardsParser();
            $input = $parser->parseFile(APP_DIR . DIRECTORY_SEPARATOR . "$file.json");
            $builder = new BoardingCardsBuilder($input, new BoardingCardFactory(new BoardingCardValidator()));
            $list = $builder->build();
        } catch (\Exception $exception) {
            $list = [];
        }

        return $list;
    }
}
