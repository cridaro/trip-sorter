<?php

namespace TripSorter\Entity;

/**
 * Boarding card test
 */
class BoardingCardTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructSuccess()
    {
        $card = new BusBoardingCard();

        self::assertInstanceOf('TripSorter\Entity\BusBoardingCard', $card);
        self::assertEquals('bus', $card->getVehicle());
    }

    public function testPopulateSuccess()
    {
        $data = new \stdClass;
        $data->departure = 'Amsterdam';
        $data->arrival = 'Bucharest';
        $data->vehicle = 'train';
        $card = new PlaneBoardingCard($data);

        self::assertInstanceOf('TripSorter\Entity\PlaneBoardingCard', $card);
        self::assertEquals('plane', $card->getVehicle());
        self::assertEquals('Amsterdam', $card->getDeparture());
        self::assertEquals('Bucharest', $card->getArrival());
    }
}
